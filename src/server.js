const express = require('express');
const app = express();
const converter = require('./converter');
const port = 3000;

// endpoint localhost:3000/
app.get('/', (req, res) => res.send("Hello"));

// endpoint localhost:3000/rgb-to-hex?red=255&green=0&blue=0
app.get('/rgb-to-Hex', (req, res) => {
    const red = parseInt(req.query.red, 10)    
    const green = parseInt(req.query.green, 10)    
    const blue = parseInt(req.query.blue, 10)    
    const hex = converter.rgbToHex(red, green, blue);
    res.send(hex);
})
// tästä alkaa uusi
app.get('/Hex-to-rgb', (req, res) => {
    const red = String(req.query.red, 16)    
    const green = String(req.query.green, 16)    
    const blue = String(req.query.blue, 16)    
    const hex = converter.HexToRGB(red, green, blue);
    res.send(RGB);
})
// tähän asti
if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(port, () => console.log(`Server listening on localhost:${port}`))
}