// ./test/server.spec.js - Integration test
const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {
    let server = undefined; // works without defining
    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost:${3000}`);
            done();
        });
    });

    describe("RGB to Hex conversion", () => {
        const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("ffffff");
                done();
            });
        });
    });
    //uusi alkaa
    describe("Hex to RGB conversion", () => {
        const url = `http://localhost:${port}/Hex-to-RGB?red=ff0000&green=00ff00&blue=0000ff`;

        it("returns status fffff", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal("fffff");
                done();
            });
        });
        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal(200);
                done();
            });
        });
    });
// tänne asti
    after("Stop server after tests", (done) => {
        server.close();
        done();
    })
});